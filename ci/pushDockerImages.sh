#!/usr/bin/env bash
echo $BITBUCKET_TAG;
echo $BITBUCKET_BRANCH;
echo $1;
if [ -z "$BITBUCKET_TAG" ]; then
    revisions=$BITBUCKET_BRANCH;
else
    revisions=$BITBUCKET_TAG;
fi
case $revisions in
    "master")
        revisions="latest";;
    "development")
        revisions="develop";;
    v*.*.*)
        revisions=$(echo "$revisions" | awk -F '.' '{printf $1 "." $2 "-latest " $1 "." $2 "." $3}');;
    *)
        # Don't push docker images in other cases
        revisions="";;
esac;
echo $revisions;
for revision in $revisions; do
    docker tag mainzelliste:test medicalinformatics/mainzelliste:$revision;
    if [ $1 -eq 0 ]; then
        docker push medicalinformatics/mainzelliste:$revision;
    fi
done
